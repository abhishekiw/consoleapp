﻿using System;

namespace ConsoleApp

{
 
    public abstract class Shape
    {
        int Number;
        int Number2;

        //method overloading example == number of parameter should be different
        public Shape()
        {
            Console.WriteLine("Without paramenter");
        }

        public Shape(int number, int number2)
        {
            Number = number;
            Number2 = number2;
        }

        public int calcArea()
        {
            return Number * Number2;
        }

        //method hiding
        public virtual void dispArea()
        {
            Console.WriteLine("The total area is:" );
        }
    }

    public class Square : Shape
    {
        public Square(int number) : base(number, number)
        {
        }

        //this will hide method dispArea of base class
        public override void dispArea()
        {
            //base.dispArea();
            Console.WriteLine("The total area of the square is:" + base.calcArea());
        }
    }

    public class Addition
    {
       public void Add()
        {
            try
            {
                Console.WriteLine("Enter a number: ");
                int num = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter next number: ");
                int num2 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Total number is: " + (num + num2));
                Console.ReadKey();
            }
            catch
            {
                Console.WriteLine("Please enter a number not a alphabet!" );
                Console.ReadKey();
            }

        }
    }

    #region
    public class Rectangle : Shape
    {
        
        public Rectangle(int number, int number2) : base(number, number2)
        {
            
        }

        public new void dispArea()
        {
            Console.WriteLine("The total area of a rectangle is:" + base.calcArea());
        }
        #region // main method
        public static void Main(string[] args)
        {
            try
            {
                //Console.WriteLine("Enter a length: ");
                //int length = Convert.ToInt32(Console.ReadLine());
                //Console.WriteLine("Enter a width: ");
                //int width = Convert.ToInt32(Console.ReadLine());
                Rectangle rectange = new Rectangle(22, 22);
                rectange.dispArea();
                //((Shape)rectangle).dispArea(); (type cast example)

                Square square = new Square(22);
                square.dispArea();
                //Console.ReadKey();
            }
            catch
            {
                Console.WriteLine("Please enter a number not a alphabet!");
                Console.ReadKey();
            }
                        

            Console.WriteLine("Do you want to perform addition? (yes/no): ");
            string answer = Console.ReadLine();
            if(answer.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
            {
                Addition addition = new Addition();
                addition.Add();
            }
            else if(answer.Equals("no", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("The process is completed.");
            }
            else
            {
                Environment.Exit(0);
            }
            Console.ReadKey();
        }
        #endregion
    }
    #endregion
}
